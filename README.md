# lms-test-upgrade
Prepare an upgrade package for an individual Totara site, then use this package to perform an (offline, local) upgrade test.
