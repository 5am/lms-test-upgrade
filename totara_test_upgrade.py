# Prepare a site-specific Totara package and perform an upgrade
# in a local test environment.

__author__ = 'Sam Howell'
__version__ = '0.1'

import subprocess
import os
import sys
import json
import re
import argparse


def main():
    def run(c):
        subprocess.run(c, shell=True)

    # Define available and required script options
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--version',
                        action='version',
                        version=f'%(prog)s {__version__}')
    parser.add_argument('-u', '--user',
                        action='store',
                        required=True,
                        metavar='user',
                        help='cPanel account username')
    parser.add_argument('-t', '--totara',
                        action='store',
                        required=True,
                        metavar='ver',
                        help='Totara version (e.g. 13.40, 18.1)')
    parser.add_argument('-p', '--php',
                        action='store',
                        required=True,
                        metavar='ver',
                        help='PHP version (e.g. 7.4, 8.0)')
    args = parser.parse_args()
    user = args.user.lower().strip()
    totara = args.totara
    php = args.php

    # Only continue if we've passed valid arguments
    try:
        if not len(php) == 3:
            raise ValueError
        elif not re.match(r'^[7-8]\.[0-9]$', php):
            raise ValueError
        elif len(totara) > 5:
            raise ValueError
        elif not re.match(r'^1[2-8](\.[1-9]|\.[1-9][0-9])?$', totara):
            raise ValueError
    except ValueError:
        print('One or more invalid arguments.')
        parser.print_help()
        sys.exit(1)
    else:
        pass

    # Only continue if the username is valid
    prog_path = os.path.dirname(os.path.realpath(__file__))
    with open(f'{prog_path}/totara-sites-config-example.json') as config_file:
        configs = json.loads(config_file.read())
        valid_users = []
        for users in configs:
            valid_users.append(str(users))
        if user not in valid_users:
            print(f'User not found.')
            sys.exit(1)
        else:
            pass

    release = f'totaratxp-{totara}.tar.gz'
    public_path = f'/home/{user}/public_html/'
    totara_path = '/home/user/totara/'
    testing_path = f'{totara_path}testing/'
    extracted_path = f'{testing_path}extracted/'
    gmods_path = f'{totara_path}mods/generic/'
    cmods_path = f'{totara_path}mods/customer/{user}/'
    releases_path = f'{totara_path}releases/'
    release_path = f'{releases_path}{release}'

    # Load the user's mods from their site config
    gmods = configs[f'{user}']['generic']
    if len(configs[f'{user}']) == 2:
        cmods = configs[f'{user}']['customer']
        gcmods = True
    else:
        gcmods = False

    print(f'\nThe following mods will be used for {user}...\n')
    print('generic:')
    for gmod in gmods:
        print(f'  {gmod}')
    if gcmods:
        print('\ncustomer:')
        for cmod in cmods:
            print(f'  {cmod}')
    print()

    proceed = str(input(f'Upgrade {user} to Totara {totara}? [y/n]: ')).lower().strip()
    if proceed[:1] != 'y':
        sys.exit(1)

    # Extract the release package, ready for addition of mods
    print('Unpacking release...')
    run(f'tar -xf {release_path} -C {extracted_path}')

    print(f'Moving mods into {totara} release...')
    # Copy the generic mods into the release package
    for gmod in gmods:
        if re.match(r'^mod_.*_totara.*$', gmod):
            print(f'{gmod} -> totaratxp-{totara}/server/mod/')
            run(f'unzip -q {gmods_path}{gmod}.zip -d {extracted_path}totaratxp-{totara}/server/mod/')
        elif re.match(r'^local_.*_totara.*$', gmod):
            print(f'{gmod} -> totaratxp-{totara}/server/local/')
            run(f'unzip -q {gmods_path}{gmod}.zip -d {extracted_path}totaratxp-{totara}/server/local/')
        elif re.match(r'^auth_.*_totara.*$', gmod):
            print(f'{gmod} -> totaratxp-{totara}/server/auth/')
            run(f'unzip -q {gmods_path}{gmod}.zip -d {extracted_path}totaratxp-{totara}/server/auth/')
        elif re.match(r'^block_.*_totara.*$', gmod):
            print(f'{gmod} -> totaratxp-{totara}/server/blocks/')
            run(f'unzip -q {gmods_path}{gmod}.zip -d {extracted_path}totaratxp-{totara}/server/blocks/')
        elif re.match(r'^qtype_.*_totara.*$', gmod):
            print(f'{gmod} -> totaratxp-{totara}/server/question/type/')
            run(f'unzip -q {gmods_path}{gmod}.zip -d {extracted_path}totaratxp-{totara}/server/question/type/')
        elif re.match(r'^report_.*_totara.*$', gmod):
            print(f'{gmod} -> totaratxp-{totara}/server/report/')
            run(f'unzip -q {gmods_path}{gmod}.zip -d {extracted_path}totaratxp-{totara}/server/report/')
    if gcmods:
        # If also customer mods, copy them into the release package
        for cmod in cmods:
            if re.match(r'^mod_certificate_type', cmod):
                print(f'{cmod}/* -> totaratxp-{totara}/server/mod/certificate/type/')
                run(f'unzip -q {cmods_path}{cmod}.zip -d {extracted_path}totaratxp-{totara}/server/mod/certificate/type/')
            elif re.match(r'^mod_.*_totara.*$', cmod):
                print(f'{cmod} -> totaratxp-{totara}/server/mod/')
                run(f'unzip -q {cmods_path}{cmod}.zip -d {extracted_path}totaratxp-{totara}/server/mod/')
            elif re.match(r'^local_.*_totara.*$', cmod):
                print(f'{cmod} -> totaratxp-{totara}/server/local/')
                run(f'unzip -q {cmods_path}{cmod}.zip -d {extracted_path}totaratxp-{totara}/server/local/')
            elif re.match(r'^auth_.*_totara.*$', cmod):
                print(f'{cmod} -> totaratxp-{totara}/server/auth/')
                run(f'unzip -q {cmods_path}{cmod}.zip -d {extracted_path}totaratxp-{totara}/server/auth/')
            elif re.match(r'^block_.*_totara.*$', cmod):
                print(f'{cmod} -> totaratxp-{totara}/server/blocks/')
                run(f'unzip -q {cmods_path}{cmod}.zip -d {extracted_path}totaratxp-{totara}/server/blocks/')
            elif re.match(r'^qtype_.*_totara.*$', cmod):
                print(f'{cmod} -> totaratxp-{totara}/server/question/type/')
                run(f'unzip -q {cmods_path}{cmod}.zip -d {extracted_path}totaratxp-{totara}/server/question/type/')
            elif re.match(r'^report_.*_totara.*$', cmod):
                print(f'{cmod} -> totaratxp-{totara}/server/report/')
                run(f'unzip -q {cmods_path}{cmod}.zip -d {extracted_path}totaratxp-{totara}/server/report/')

    # Prepare the package for later download and use for live upgrade
    print('Archiving upgrade package to uptmp...')
    os.chdir(f'{extracted_path}totaratxp-{totara}')
    run(f'tar -czf /home/uptmp/{user}-totaratxp-{totara}.tar.gz .')

    print('Removing old source...')
    run(f'sudo cp {public_path}config.php {extracted_path}totaratxp-{totara}/')
    run(f'sudo rm -rf {public_path}*')

    print('Copying new source into place...')
    run(f'sudo cp -r {extracted_path}totaratxp-{totara}/* {public_path}')
    run(f'sudo chown -R apache:apache {public_path}')

    # We want, e.g., 74, 80, 81, for the relevant Remi PHP binary
    php = php.replace('.', '')

    print('Enabling maintenance mode...')
    run(f'php{php} {public_path}server/admin/cli/maintenance.php --enable')

    print('Upgrading site...')
    run(f'php{php} {public_path}server/admin/cli/upgrade.php')

    print('Purging caches...')
    run(f'php{php} {public_path}server/admin/cli/purge_caches.php')

    print('Disabling maintenance mode...')
    run(f'php{php} {public_path}server/admin/cli/maintenance.php --disable')

    # Check the site, compare exported reports etc. before continuing
    cron = str(input('Continue to cron run? [y/n]: ')).lower().strip()
    if cron[:1] != 'y':
        sys.exit(1)

    def cron():
        run(f'php{php} {public_path}server/admin/cli/cron.php')
    run_cron = 'y'
    # Run cron until all tasks are complete
    while run_cron[:1] == 'y':
        print('Running cron...')
        cron()
        run_cron = str(input('Run cron again? [y/n]: ')).lower().strip()

    sys.exit(0)


if __name__ == '__main__':
    main()
